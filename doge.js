$(document).ready(function(){
    dogeify();
});

function dogeify(){

    var adjectives = location.hash.substr(1).split(',') // for url parameters it would be location.search
    
    var caption_data = generate_captions(adjectives);
    
    // Inject captions in DOM
    for ( i in caption_data){
        var caption = caption_data[i];
        var p = $("<p class='dogeText'></p>");
        p.html(caption.text);
        p.css( {
            color: caption.color,
            top: caption.pos_y+"px",
            left: caption.pos_x+"px",
        });
        $('body').append(p);
    }
}

function generate_captions(adjectives){
    
    var caption_data = [];
    
    // Initially seed texts with the desired adjectives
    for ( i in adjectives){
        var adjective = adjectives[i];
        caption_data.push({'text': adjective});
    }
    
    // Incrementally generate captions
    caption_data = 
        generate_colors(
            generate_positions(
                generate_texts(caption_data)));
        
    return caption_data;
}

function generate_texts(caption_data){

    var augmenters = ["so", "much", "such", "very"];
    
    for ( i in caption_data){
        var adjective = caption_data[i].text;
        augmenter = augmenters[Math.floor(Math.random()*augmenters.length)];
        caption_data[i].text = augmenter+" "+adjective;
    }    
    
    caption_data.push({'text': "wow"});
            
    return caption_data;
    
}

function generate_positions(caption_data){
    
    for ( i in caption_data){
        var caption = caption_data[i];

        caption.pos_x = Math.floor(Math.random()*$(window).width());
        caption.pos_y = Math.floor(Math.random()*$(window).height());
    }    
    
    return caption_data;
            
}

function generate_colors(caption_data){
    
    function random_color(){
        // vivid colors, just with 0's and f's
        colorstr = '#';
        for(i=0;i<6;i++){
            seed = Math.floor(Math.random()*10) ;
            hexdig = seed % 2 == 0 ? '0' : 'f' ;
            colorstr += hexdig;
        }
        return colorstr;
    }
    
    for ( i in caption_data){
        var caption = caption_data[i];
        caption.color = random_color();
    }    
    
    return caption_data;
}


